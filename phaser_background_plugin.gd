extends EditorPlugin
tool

var editor_interface = null

func get_name(): 
	return "PhaserBackground"

func _enter_tree():
	editor_interface = get_editor_interface()
	
	add_custom_type("PhaserBackground", "Control", preload("phaser_background.gd"), editor_interface.get_base_control().get_icon("ColorRect", "EditorIcons"))

func _exit_tree():
	remove_custom_type("PhaserBackground")