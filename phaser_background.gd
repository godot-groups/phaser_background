extends Control
tool

var phaser_effect = null

const phaser_effect_const = preload("phaser_effect.gd")
const firefly_particles_const = preload("firefly_particles.gd")

export(Color) var top_color = Color(0.95, 0.95, 0.95, 1.0) setget set_top_color
export(Color) var bottom_color = Color(0.36, 0.73, 0.87, 1.0) setget set_bottom_color

func set_top_color(p_color):
	top_color = p_color
	if phaser_effect:
		phaser_effect.top_left_color = top_color
		phaser_effect.top_right_color = top_color
		
func set_bottom_color(p_color):
	bottom_color = p_color
	if phaser_effect:
		phaser_effect.bottom_left_color = bottom_color
		phaser_effect.bottom_right_color = bottom_color

func _ready():
	phaser_effect = phaser_effect_const.new()
	phaser_effect.set_name("PhaserEffect")
	add_child(phaser_effect)
	
	phaser_effect.set_anchors_and_margins_preset(PRESET_WIDE, PRESET_MODE_MINSIZE)
	
	phaser_effect.top_left_color = top_color
	phaser_effect.top_right_color = top_color
	phaser_effect.bottom_left_color = bottom_color
	phaser_effect.bottom_right_color = bottom_color
	
	var firefly_particles = firefly_particles_const.new()
	firefly_particles.set_name("FireflyParticles")
	connect("resized", firefly_particles, "_resized")
	add_child(firefly_particles)