extends "res://addons/canvas_extension/blended_color_rect.gd"
tool

var time = 0.0

func _ready():
	if(Engine.is_editor_hint() == false):
		set_process(true)
	
func _process(p_delta):
	time += p_delta
	
	var wave = 1.25 + sin(time * 0.50) * 0.25
	ignore_resize = true # Prevents rescaling requring the canvas's points to be reconstructed
	set_scale(Vector2(1.0, wave))
	ignore_resize = false