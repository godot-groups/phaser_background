extends Particles2D
tool

export(bool) var run_once = false
	
func resize_particles():
	set_position(get_parent().get_size() * 0.5)
	
	var smallest
	if(get_parent().get_size().x < get_parent().get_size().y):
		smallest = get_parent().get_size().x
	else:
		smallest = get_parent().get_size().y
		
	var custom_scale = smallest / 64
	set_scale(Vector2(custom_scale, custom_scale))
	
	var particle_emission_extents_size = (get_parent().get_size() / 2) / custom_scale
	
	if process_material:
		process_material.set_emission_box_extents(Vector3(particle_emission_extents_size.x, particle_emission_extents_size.y, 0.0))

func _resized():
	resize_particles()

func _on_visibility_changed():
	if(is_visible()):
		set_emitting(true)
		if(run_once):
			restart()
	else:
		set_emitting(false)
		restart()
		
func _ready():
	run_once = true
	
	connect("visibility_changed", self, "_on_visibility_changed")
	
	# Setup default state
	emitting = true
	amount = 32
	
	lifetime = 2
	one_shot = false
	preprocess = 10
	speed_scale = 0.1
	explosiveness = 0
	randomness = 0
	fixed_fps = 0
	fract_delta = 0
	
	local_coords = true
	draw_order = Particles2D.DRAW_ORDER_INDEX
	
	process_material = load("res://addons/phaser_background/firefly_particles_material.tres")
	resize_particles()
	
	texture = load("res://addons/phaser_background/firefly.png")
	
	restart()